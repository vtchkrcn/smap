package eu.edoo.dashboard;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import eu.edoo.dashboard.task.TaskService;
import eu.edoo.dashboard.task.adapter.TaskAdapter;
import eu.edoo.dashboard.task.model.Task;

public class MainActivity extends AppCompatActivity {

    public static final int DELAY = 30000;

    private TaskService service;

    private RecyclerView recyclerView;
    private TaskAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private int maxId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recycler_view);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new TaskAdapter();
        recyclerView.setAdapter(adapter);

        checkNetwork();

        service = new TaskService(this);
        service.load(maxId);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable(){
            public void run(){
                service.load(maxId);
                handler.postDelayed(this, DELAY);
            }
        }, DELAY);
    }

    public void displayTasks(List<Task> tasks) {
        adapter.addTasks(tasks);
        adapter.notifyDataSetChanged();
        updateMaxId(tasks);
    }

    private void updateMaxId(List<Task> tasks) {
        if (tasks.size() > 0) {
            Task task = tasks.get(0);
            maxId = task.getId();
        }
        Log.d(MainActivity.class.toString(), "Maximum ID is now: " + maxId + ".");
    }

    private void checkNetwork() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() == null) {
            Toast.makeText(getApplicationContext(), "App needs internet connection to work properly.", Toast.LENGTH_LONG).show();
        }
    }
}
