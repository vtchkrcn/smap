package eu.edoo.dashboard.task;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import eu.edoo.dashboard.MainActivity;
import eu.edoo.dashboard.R;
import eu.edoo.dashboard.task.model.Task;

public class TaskService {

    private static final String URL = "http://www.vtchkrcn.cz/api/dashboard-load/";
    private static final String ENCODING = "utf-8";

    private String API_KEY;

    private MainActivity activity;

    public TaskService(MainActivity activity) {
        this.activity = activity;
        API_KEY = this.activity.getString(R.string.vtchkrcn_api_key);
    }

    public void load(int minId) {
        try {
            RequestQueue queue = Volley.newRequestQueue(activity);

            Response.Listener<String> responseListener = new Response.Listener<String>() {
                @Override
                public void onResponse(String data) {
                    Log.d(Response.Listener.class.toString(), "Response from server: " + data);

                    List<Task> tasks = new ArrayList<>();

                    try {
                        ObjectMapper mapper = new ObjectMapper();
                        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                        mapper.setDateFormat(format);
                        tasks = mapper.readValue(data, new TypeReference<List<Task>>(){});
                    } catch (IOException ex) {
                        Log.e(Response.Listener.class.toString(), ex.getMessage());
                        ex.printStackTrace();
                    }

                    activity.displayTasks(tasks);
                }
            };

            Response.ErrorListener errorListener = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    String message = error.getMessage();
                    Log.e(Response.ErrorListener.class.toString(), message.isEmpty() ? "Unspecified error." : message);
                    error.printStackTrace();
                }
            };

            String url = createUrl(minId);
            Log.d(TaskService.class.toString(), url);

            StringRequest stringRequest = new StringRequest(Request.Method.GET, url, responseListener, errorListener);

            queue.add(stringRequest);
        } catch (UnsupportedEncodingException ex) {
            Log.e(TaskService.class.toString(), ex.getMessage());
            ex.printStackTrace();
        }
    }

    private String createUrl(int minId) throws UnsupportedEncodingException {
        StringBuilder builder = new StringBuilder(URL);

        builder.append("?apiKey=");
        builder.append(URLEncoder.encode(API_KEY, ENCODING));

        builder.append("&minId=");
        builder.append(minId);

        return builder.toString();
    }
}
