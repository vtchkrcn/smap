<?php
/**
 * Created on 19.03.2019.
 * @author vtchkrcn <kalivodav@gmail.com>
 */

namespace App\Presenters;


use App\Repository\TaskRepository;

class TaskPresenter extends BasePresenter
{
    /** @var TaskRepository */
    private $taskRepository;

    public function __construct(TaskRepository $taskRepository)
    {
        parent::__construct();
        $this->taskRepository = $taskRepository;
    }

    public function actionDefault()
    {
        $template = $this->template;
        $template->tasks = $this->taskRepository->findAll();
    }
}