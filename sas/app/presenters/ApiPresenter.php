<?php
/**
 * Created on 19.12.2018.
 * @author vtchkrcn <kalivodav@gmail.com>
 */

namespace App\Presenters;


use App\Security\BasicSecurity;
use App\Security\Exception\SecurityException;
use App\Service\CalendarService;
use App\Repository\TaskRepository;
use Nette\Application\Responses\JsonResponse;

class ApiPresenter extends BasePresenter
{
    const PARAM_API_KEY = 'apiKey';
    const PARAM_CONTENT = 'content';
    const PARAM_DATE = 'date';

    /** @var BasicSecurity */
    private $basicSecurity;

    /** @var TaskRepository */
    private $taskRepository;

    /** @var CalendarService */
    private $calendarService;


    public function __construct(TaskRepository $taskRepository, BasicSecurity $basicSecurity, CalendarService $calendarService)
    {
        parent::__construct();
        $this->taskRepository = $taskRepository;
        $this->basicSecurity = $basicSecurity;
        $this->calendarService = $calendarService;
    }


    protected function startup()
    {
        parent::startup();
        try {
            $apiKey = $this->getHttpRequest()->getQuery(self::PARAM_API_KEY);
            if (!$apiKey) {
                throw new SecurityException('Missing parameter ' . self::PARAM_API_KEY . '.');
            }
            $this->basicSecurity->checkApiKey($apiKey);
        } catch(SecurityException $ex) {
            $this->sendResponse(new JsonResponse(['result' => $ex->getMessage()]));
        }
    }

    /**
     * Method for saving event.
     */
    public function actionEvent()
    {
        $content = $this->getHttpRequest()->getPost(self::PARAM_CONTENT);
        $date = $this->getHttpRequest()->getPost(self::PARAM_DATE);
        // Validations
        $error = [];
        if (!$content) {
            $error[] = self::PARAM_CONTENT;
        }
        if (!$date) {
            $error[] = self::PARAM_DATE;
        }
        if (!empty($error)) {
            $this->sendResponse(new JsonResponse(['result' => 'Missing parameter ' . implode(', ', $error) . '.']));
        }

        // Calendar api check
        if (!$this->calendarService->checkAccess()) {
            $this->sendResponse(new JsonResponse(['result' => 'Cannot connect to calendar api.']));
        }

        $this->taskRepository->save([
            TaskRepository::COLUMN_TYPE => TaskRepository::TYPE_EVENT,
            TaskRepository::COLUMN_CONTENT => $content,
            TaskRepository::COLUMN_EVENT_DATE => $date
        ]);
        $this->calendarService->insert($content, $date);

        $this->sendResponse(new JsonResponse(['result' => 'Event has been created.']));
    }


    /**
     * Method for saving note.
     */
    public function actionNote()
    {
        $content = $this->getHttpRequest()->getPost(self::PARAM_CONTENT);
        // Validations
        if (!$content) {
            $this->sendResponse(new JsonResponse(['result' => 'Missing parameter ' . self::PARAM_CONTENT . '.']));
        }

        $this->taskRepository->save([
            TaskRepository::COLUMN_TYPE => TaskRepository::TYPE_NOTE,
            TaskRepository::COLUMN_CONTENT => $content
        ]);
        $this->sendResponse(new JsonResponse(['result' => 'Note has been saved.']));
    }


    /**
     * Method for saving dashboard note.
     */
    public function actionDashboard()
    {
        $content = $this->getHttpRequest()->getPost(self::PARAM_CONTENT);
        // Validations
        if (!$content) {
            $this->sendResponse(new JsonResponse(['result' => 'Missing parameter ' . self::PARAM_CONTENT . '.']));
        }

        $this->taskRepository->save([
            TaskRepository::COLUMN_TYPE => TaskRepository::TYPE_DASHBOARD,
            TaskRepository::COLUMN_CONTENT => $content
        ]);
        $this->sendResponse(new JsonResponse(['result' => 'Message was sent to dashboard.']));
    }


    /**
     * Loading method for dashboard.
     *
     * @param int $minId
     */
    public function actionDashboardLoad($minId)
    {
        $dashboardNotes = null;
        if (!$minId) {
            $dashboardNotes = $this->taskRepository->findAllByType(TaskRepository::TYPE_DASHBOARD);
        } else {
            $dashboardNotes = $this->taskRepository->findAllByTypeAndMinId(TaskRepository::TYPE_DASHBOARD, $minId);
        }

        $result = [];
        foreach ($dashboardNotes as $note) {
            $result[] = [
                'id' => $note->id,
                'createdAt' => date('Y-m-d H:i', strtotime($note->created_at)),
                'content' => $note->content
            ];
        }

        $this->sendResponse(new JsonResponse($result));
    }
}