<?php

namespace App\Presenters;


use App\Service\CalendarService;
use App\Repository\TaskRepository;
use Nette\Application\UI\Form;
use App\Security\BasicSecurity;

final class HomepagePresenter extends BasePresenter
{
    /** @var TaskRepository */
    private $repository;

    /** @var BasicSecurity */
    private $basicSecurity;

    /** @var CalendarService */
    private $calendarService;

    public function __construct(TaskRepository $repository, BasicSecurity $basicSecurity, CalendarService $calendarService)
    {
        parent::__construct();
        $this->repository = $repository;
        $this->basicSecurity = $basicSecurity;
        $this->calendarService = $calendarService;
    }

    public function actionDefault()
	{
	    $template = $this->template;
        $template->notes = $this->repository->findAllByType(TaskRepository::TYPE_NOTE);

	    $refreshToken = false;
        if (!$this->calendarService->checkAccess()) {
            $refreshToken = true;
        }
        $template->refreshToken = $refreshToken;
    }

    /**
     * @return Form
     */
	protected function createComponentForm()
    {
        $form = new Form();
        $form->addPassword('pass', 'Pass:')
            ->setRequired('Insert password.');
        $form->addSubmit('submit', 'Submit');
        $form->onSuccess[] = [$this, 'formSucceeded'];
        return $form;
    }

    /**
     * @param Form $form
     * @param \stdClass $values
     */
    public function formSucceeded(Form $form, \stdClass $values)
    {
        if (!$this->basicSecurity->checkTokenRefreshPass($values->pass)) {
            $this->flashMessage('Invalid password.', 'alert');
            return;
        }
        $this->redirect('Refresh:');
    }
}
