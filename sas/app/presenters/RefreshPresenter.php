<?php
/**
 * Created on 08.01.2019.
 * @author vtchkrcn <kalivodav@gmail.com>
 */

namespace App\Presenters;


use App\Security\BasicSecurity;
use App\Service\CalendarService;
use App\Service\TokenRefreshService;
use Nette\Application\UI\Form;

class RefreshPresenter extends BasePresenter
{
    /** @var TokenRefreshService */
    private $tokenRefreshService;

    /** @var BasicSecurity */
    private $basicSecurity;

    /** @var CalendarService */
    private $calendarService;


    public function __construct(TokenRefreshService $tokenRefreshService, BasicSecurity $basicSecurity, CalendarService $calendarService)
    {
        parent::__construct();
        $this->tokenRefreshService = $tokenRefreshService;
        $this->basicSecurity = $basicSecurity;
        $this->calendarService = $calendarService;
    }


    public function actionDefault()
    {
        $template = $this->template;
        $template->link = $this->calendarService->getGoogleService()->getClient()->createAuthUrl();
    }


    /**
     * @return Form
     */
    protected function createComponentForm()
    {
        $form = new Form();
        $form->addText('code', 'Code:')
            ->setRequired('Insert code.');
        $form->addPassword('pass', 'Pass:')
            ->setRequired('Insert password.');
        $form->addSubmit('submit', 'Submit');
        $form->onSuccess[] = [$this, 'formSucceeded'];
        return $form;
    }

    /**
     * @param Form $form
     * @param \stdClass $values
     */
    public function formSucceeded(Form $form, \stdClass $values)
    {
        if (!$this->basicSecurity->checkTokenRefreshPass($values->pass)) {
            $this->flashMessage('Invalid password.', 'alert');
            return;
        }
        $authCode = $values->code;
        $this->tokenRefreshService->fetchNewToken($authCode);
        $this->flashMessage('Token has been successfully fetched and saved.');
        $this->redirect('Homepage:');
    }
}