<?php
/**
 * Created on 08.01.2019.
 * @author vtchkrcn <kalivodav@gmail.com>
 */

namespace App\Inter;


interface CredentialsInterface
{
    const APPLICATION_NAME = 'Smart Assistant';

    const PATH_CREDENTIALS = __DIR__ . '/../credentials/credentials.json';
    const PATH_TOKEN = __DIR__ . '/../credentials/token.json';
}