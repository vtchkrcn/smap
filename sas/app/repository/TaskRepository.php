<?php
/**
 * Created on 19.12.2018.
 * @author vtchkrcn <kalivodav@gmail.com>
 */

namespace App\Repository;

use Nette;

class TaskRepository
{
    const TABLE = 'task';

    const COLUMN_ID = 'id';
    const COLUMN_CREATED_AT = 'created_at';
    const COLUMN_TYPE = 'type';
    const COLUMN_CONTENT = 'content';
    const COLUMN_EVENT_DATE = 'event_date';

    const TYPE_NOTE = 'note';
    const TYPE_DASHBOARD = 'dashboard';
    const TYPE_EVENT = 'event';

    /** @var Nette\Database\Context */
    private $database;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    /**
     * @return Nette\Database\Table\ActiveRow
     */
    public function findAll()
    {
        return $this->database->table(self::TABLE)->order(self::COLUMN_CREATED_AT.' DESC')->fetchAll();
    }


    /**
     * @param array $task
     */
    public function save($task)
    {
        $this->database->table(self::TABLE)->insert($task);
    }


    /**
     * @param int $limit
     * @return Nette\Database\Table\ActiveRow
     */
    public function findAllByLimit($limit = 50)
    {
        return $this->database->table(self::TABLE)->order(self::COLUMN_CREATED_AT.' DESC')->limit($limit)->fetchAll();
    }


    /**
     * @param string $type
     * @param int $limit
     * @return Nette\Database\Table\ActiveRow
     */
    public function findAllByType($type, $limit = 50)
    {
        return $this->database
            ->table(self::TABLE)
            ->where(self::COLUMN_TYPE, $type)
            ->order(self::COLUMN_CREATED_AT.' DESC')
            ->limit($limit)
            ->fetchAll();
    }

    /**
     * @param string $type
     * @param int $minId
     * @return Nette\Database\Table\ActiveRow
     */
    public function findAllByTypeAndMinId($type, $minId)
    {
        return $this->database
            ->table(self::TABLE)
            ->where(self::COLUMN_TYPE, $type)
            ->where(self::COLUMN_ID . " > ?", $minId)
            ->order(self::COLUMN_CREATED_AT.' DESC')
            ->fetchAll();
    }
}