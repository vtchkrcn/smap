<?php
/**
 * Created on 08.01.2019.
 * @author vtchkrcn <kalivodav@gmail.com>
 */

namespace App\Service;


use App\Factory\GoogleClientFactory;
use App\Inter\CredentialsInterface;

class TokenRefreshService implements CredentialsInterface
{
    /** @var GoogleClientFactory */
    private $factory;


    public function __construct(GoogleClientFactory $factory)
    {
        $this->factory = $factory;
    }


    /**
     * @param string $authCode
     * @throws \Exception
     */
    public function fetchNewToken($authCode)
    {
        $client = $this->factory->create();
        $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
        $client->setAccessToken($accessToken);

        if (array_key_exists('error', $accessToken)) {
            throw new \Exception(join(', ', $accessToken));
        }

        // Save the token to a file.
        if (!file_exists(dirname(self::PATH_TOKEN))) {
            mkdir(dirname(self::PATH_TOKEN), 0700, true);
        }
        file_put_contents(self::PATH_TOKEN, json_encode($client->getAccessToken()));
    }
}