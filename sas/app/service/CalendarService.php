<?php
/**
 * Created on 06.01.2019.
 * @author vtchkrcn <kalivodav@gmail.com>
 */

namespace App\Service;



use App\Factory\GoogleServiceFactory;

class CalendarService
{
    const CALENDAR_ID = 'primary';

    /** @var \Google_Service_Calendar */
    private $service;


    public function __construct(GoogleServiceFactory $factory)
    {
        $this->service = $factory->create();
    }


    /**
     * @return \Google_Service_Calendar
     */
    public function getGoogleService()
    {
        return $this->service;
    }


    /**
     * @param string $content
     * @param string $eventDate
     */
    public function insert($content, $eventDate)
    {
        $datetime = new \DateTime();
        $event = new \Google_Service_Calendar_Event([
            'summary' => $content,
            'description' => '',
            'start' => [
                'dateTime' => date('c', strtotime($eventDate)),
                'timeZone' => $datetime->getTimezone()->getName()
            ],
            'end' => [
                'dateTime' => date('c', strtotime('+1 hour', strtotime($eventDate))),
                'timeZone' => $datetime->getTimezone()->getName()
            ]
        ]);
        $this->service->events->insert(self::CALENDAR_ID, $event);
    }


    /**
     * @return bool
     */
    public function checkAccess()
    {
        $client = $this->service->getClient();

        // If there is no previous token or it's expired.
        if ($client->isAccessTokenExpired()) {
            // Refresh the token if possible, else fetch a new one.
            if ($client->getRefreshToken()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            } else {
                return false;
            }
        }
        return true;
    }
}