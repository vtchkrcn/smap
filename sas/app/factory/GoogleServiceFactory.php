<?php
/**
 * Created on 08.01.2019.
 * @author vtchkrcn <kalivodav@gmail.com>
 */

namespace App\Factory;


use App\Service\Exception\CalendarException;

class GoogleServiceFactory
{
    /** @var GoogleClientFactory */
    private $factory;


    public function __construct(GoogleClientFactory $factory)
    {
        $this->factory = $factory;
    }


    /**
     * @return \Google_Service_Calendar
     * @throws CalendarException
     */
    public function create()
    {
        return new \Google_Service_Calendar($this->factory->create());
    }
}