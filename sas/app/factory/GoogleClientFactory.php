<?php
/**
 * Created on 08.01.2019.
 * @author vtchkrcn <kalivodav@gmail.com>
 */

namespace App\Factory;


use App\Inter\CredentialsInterface;
use App\Service\Exception\CalendarException;

class GoogleClientFactory implements CredentialsInterface
{
    /**
     * @throws CalendarException
     * @return \Google_Client
     */
    public function create()
    {
        $client = new \Google_Client();
        $client->setApplicationName(self::APPLICATION_NAME);
        $client->setAuthConfig(self::PATH_CREDENTIALS);
        $client->setScopes(\Google_Service_Calendar::CALENDAR_EVENTS);

        $httpClient = new \GuzzleHttp\Client(array('curl' => [CURLOPT_SSL_VERIFYPEER => false]));
        $client->setHttpClient($httpClient);

        if (file_exists(self::PATH_TOKEN)) {
            $accessToken = json_decode(file_get_contents(self::PATH_TOKEN), true);
            $client->setAccessToken($accessToken);
        }

        return $client;
    }
}