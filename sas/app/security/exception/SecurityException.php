<?php
/**
 * Created on 08.01.2019.
 * @author vtchkrcn <kalivodav@gmail.com>
 */
namespace App\Security\Exception;

class SecurityException extends \Exception
{
}