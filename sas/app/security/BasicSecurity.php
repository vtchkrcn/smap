<?php
/**
 * Created on 08.01.2019.
 * @author vtchkrcn <kalivodav@gmail.com>
 */
namespace App\Security;

use App\Security\Exception\SecurityException;
use Nette\Neon\Neon;

class BasicSecurity
{
    const PATH_FILE = __DIR__ . "/../credentials/keys.neon";

    const KEY_API = 'api_key';
    const KEY_REFRESH_TOKEN = 'token_refresh_pass';

    /** @var array */
    private $keys;

    /**
     * ApiSecurity constructor.
     * @throws SecurityException
     */
    public function __construct()
    {
        if (!file_exists(self::PATH_FILE)) {
            throw new SecurityException('Missing security file.');
        }

        $keys = Neon::decode(file_get_contents(self::PATH_FILE));
        $required = [self::KEY_API, self::KEY_REFRESH_TOKEN];
        foreach($required as $key) {
            if (!isset($keys[$key])) {
                throw new SecurityException("Missing {$key} in security file.");
            }
        }
        $this->keys = $keys;
    }

    /**
     * @param string $key
     * @throws SecurityException
     */
    public function checkApiKey($key)
    {
        if ($this->keys[self::KEY_API] != $key) {
            throw new SecurityException('Invalid api key.');
        }
    }

    /**
     * @param string $pass
     * @return bool
     */
    public function checkTokenRefreshPass($pass)
    {
        return ($this->keys[self::KEY_REFRESH_TOKEN] == $pass);
    }
}