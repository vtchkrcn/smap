package cz.vtchkrcn.smartassistant.util;

public class Dimensions {

    private static int SIZE_01 = 1024;
    private static int SIZE_02 = 768;

    private int width = SIZE_01;
    private int height = SIZE_02;

    public void swap() {
        width = SIZE_02;
        height = SIZE_01;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
