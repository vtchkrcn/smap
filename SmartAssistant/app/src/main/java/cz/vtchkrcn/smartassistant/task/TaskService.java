package cz.vtchkrcn.smartassistant.task;

import android.app.Activity;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import cz.vtchkrcn.smartassistant.R;
import cz.vtchkrcn.smartassistant.TaskActivity;
import cz.vtchkrcn.smartassistant.task.model.Dashboard;
import cz.vtchkrcn.smartassistant.task.model.Event;
import cz.vtchkrcn.smartassistant.task.model.Note;
import cz.vtchkrcn.smartassistant.task.model.Task;

public class TaskService {

    private static final String URL = "http://www.vtchkrcn.cz/api/";
    private static final String ENCODING = "utf-8";

    private static final String ENDPOINT_NOTE = "note";
    private static final String ENDPOINT_DASHBOARD = "dashboard";
    private static final String ENDPOINT_EVENT = "event";

    private String API_KEY;

    private TaskActivity activity;

    public TaskService(TaskActivity activity) {
        this.activity = activity;
        API_KEY = activity.getString(R.string.vtchkrcn_api_key);
    }

    public void send(final Task task) {
        RequestQueue queue = Volley.newRequestQueue(activity);
        try {
            Response.Listener<String> responseListener = new Response.Listener<String>() {
                @Override
                public void onResponse(String data) {
                    Log.d(Response.Listener.class.toString(), "Response from server: " + data);
                    String result;
                    try {
                        JSONObject jObject = new JSONObject(data);
                        result = jObject.getString("result");
                    } catch (JSONException ex) {
                        Log.e(Response.Listener.class.toString(), ex.getMessage());
                        ex.printStackTrace();
                        result = "Server responded with error. Message has not been saved.";
                    }
                    activity.processResult(Activity.RESULT_OK, result);
                }
            };

            Response.ErrorListener errorListener = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    String message = error.getMessage();
                    Log.e(Response.ErrorListener.class.toString(), message.isEmpty() ? "Unspecified error." : message);
                    error.printStackTrace();
                }
            };

            String url = createUrl(task);
            Log.d(TaskService.class.toString(), "Sending message: " + url);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, responseListener, errorListener){
                @Override
                protected Map<String, String> getParams() {
                    return TaskService.this.getParams(task);
                }
            };

            queue.add(stringRequest);
        } catch (UnsupportedEncodingException | IllegalArgumentException ex) {
            Log.e(TaskService.class.toString(), ex.getMessage());
            ex.printStackTrace();
            activity.processResult(Activity.RESULT_CANCELED, "Error during processing task data.");
        }
    }

    private String createUrl(Task task) throws UnsupportedEncodingException {
        StringBuilder builder = new StringBuilder(URL);

        builder.append(getEndPoint(task));

        builder.append("?apiKey=");
        builder.append(URLEncoder.encode(API_KEY, ENCODING));

        return builder.toString();
    }

    private String getEndPoint(Task task) {
        String endPoint;
        if (task instanceof Dashboard) {
            endPoint = ENDPOINT_DASHBOARD;
        } else if (task instanceof Event) {
            endPoint = ENDPOINT_EVENT;
        } else {
            endPoint = ENDPOINT_NOTE;
        }
        return endPoint + "/";
    }

    private Map<String, String> getParams(Task task) {
        Map<String, String> params = new HashMap<String, String>();
        if (task instanceof Dashboard) {
            Dashboard dashboard = (Dashboard) task;
            params.put("content", dashboard.getContent());
        } else if (task instanceof Event) {
            Event event = (Event) task;
            params.put("content", event.getContent());
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            params.put("date", format.format(event.getDate()));
        } else if (task instanceof Note) {
            Note note = (Note) task;
            params.put("content", note.getContent());
        } else {
            throw new IllegalArgumentException("Cannot create url query. Unsupported type of task.");
        }
        return params;
    }
}
