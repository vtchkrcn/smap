package cz.vtchkrcn.smartassistant.task.model;

import java.util.Date;

public class Event extends Note {

    private Date date;

    public Event(String content, Date date) {
        super(content);
        this.date = date;
    }

    public Date getDate() {
        return date;
    }
}
