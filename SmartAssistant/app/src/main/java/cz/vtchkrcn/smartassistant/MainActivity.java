package cz.vtchkrcn.smartassistant;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cz.vtchkrcn.smartassistant.util.Dimensions;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_CAMERA = 1;
    private static final int REQUEST_DETECT = 2;

    private Button cameraButton;

    private File bigPhoto;
    private List<File> files = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkNetwork();

        cameraButton = findViewById(R.id.camera_button);
        cameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (cameraIntent.resolveActivity(getPackageManager()) != null) {
                    try {
                        bigPhoto = createImageFile();
                        Uri photoURI = FileProvider.getUriForFile(MainActivity.this, "com.example.android.fileprovider", bigPhoto);
                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        startActivityForResult(cameraIntent, REQUEST_CAMERA);
                    } catch (IOException ex) {
                        Log.e(MainActivity.class.toString(), "Error during creating file for camera image.");
                        ex.printStackTrace();
                        showToast("Cannot save photo.");
                    }
                } else {
                    showToast("Cannot open the camera.");
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CAMERA ) {
            if (resultCode == RESULT_OK && bigPhoto.exists()) {
                try {
                    Bitmap bigBitmap = BitmapFactory.decodeFile(bigPhoto.getAbsolutePath());
                    Dimensions dimensions = new Dimensions();
                    if (bigBitmap.getWidth() < bigBitmap.getHeight()) {
                        dimensions.swap();
                    }
                    Bitmap smallBitmap = Bitmap.createScaledBitmap(bigBitmap, dimensions.getWidth(), dimensions.getHeight(), true);
                    File previewPhoto = createImageFile();
                    FileOutputStream fos = new FileOutputStream(previewPhoto);
                    smallBitmap.compress(Bitmap.CompressFormat.PNG, 90, fos);
                    fos.close();
                    bigPhoto.delete();
                    Log.d(MainActivity.class.toString(), "Image has been created. Width: " + smallBitmap.getWidth() + ", height: " + smallBitmap.getHeight());
                    startDetection(previewPhoto);
                } catch (IOException ex) {
                    ex.printStackTrace();
                    Log.e(MainActivity.class.toString(), "Error during scaling image.");
                    showToast("Error during scaling image.");
                }
            }  else {
                showToast("Photo has not been saved.");
            }
        } else if (requestCode == REQUEST_DETECT) {
            deleteFiles();
            if (data != null) {
                String result = data.getStringExtra("result");
                showToast(result);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        deleteFiles();
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "photo_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
            imageFileName,
            ".png",
            storageDir
        );
        files.add(image);
        return image;
    }

    private void startDetection(File photo) {
        Intent intent = new Intent(MainActivity.this, DetectionActivity.class);
        intent.putExtra("path", photo.getAbsolutePath());
        startActivityForResult(intent, REQUEST_DETECT);
    }

    private void showToast(String text) {
        Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
    }

    private void deleteFiles() {
        for (File file : files) {
            if (file.exists()) {
                file.delete();
            }
        }
        files.clear();
    }

    private void checkNetwork() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() == null) {
            showToast("App needs internet connection to work properly.");
        }
    }
}
