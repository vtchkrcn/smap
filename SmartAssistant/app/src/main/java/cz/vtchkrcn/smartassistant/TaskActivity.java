package cz.vtchkrcn.smartassistant;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import cz.vtchkrcn.smartassistant.task.TaskFactory;
import cz.vtchkrcn.smartassistant.task.TaskService;
import cz.vtchkrcn.smartassistant.task.model.Dashboard;
import cz.vtchkrcn.smartassistant.task.model.Event;
import cz.vtchkrcn.smartassistant.task.model.Note;
import cz.vtchkrcn.smartassistant.task.model.Task;

public class TaskActivity extends AppCompatActivity {

    private Task task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);

        final TaskService service = new TaskService(this);

        Button sendButton = findViewById(R.id.send_button);
        TextView contentText = findViewById(R.id.content_text);
        TextView typeText = findViewById(R.id.task_type_text);
        TextView dateText = findViewById(R.id.date_text);

        Intent intent = getIntent();
        String data = intent.getStringExtra("data");
        try {
            task = new TaskFactory().create(data);
        } catch (ParseException ex) {
            Log.e(TaskActivity.class.toString(), ex.getMessage());
            ex.printStackTrace();
            processResult(RESULT_CANCELED, "Bad time format.");
        }

        if (task instanceof Dashboard) {
            typeText.setText("Dashboard");
            Dashboard dashboard = (Dashboard) task;
            contentText.setText(dashboard.getContent());
        } else if (task instanceof Event) {
            typeText.setText("Event");
            Event event = (Event) task;
            contentText.setText(event.getContent());

            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm");
            dateText.setText(format.format(event.getDate()));
            dateText.setVisibility(View.VISIBLE);
            TextView dateLabel = findViewById(R.id.date_label_text);
            dateLabel.setVisibility(View.VISIBLE);
        } else if (task instanceof Note) {
            typeText.setText("Note");
            Note note = (Note) task;
            contentText.setText(note.getContent());
        } else {
            sendButton.setEnabled(false);
            sendButton.setVisibility(View.INVISIBLE);
        }

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                service.send(task);
            }
        });
    }

    public void processResult(int success, String result) {
        Intent intent = new Intent();
        intent.putExtra("result", result);
        setResult(success, intent);
        finish();
    }
}
