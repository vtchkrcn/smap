package cz.vtchkrcn.smartassistant.task.model;

public class Note extends Task {

    private String content;

    public Note(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }
}
