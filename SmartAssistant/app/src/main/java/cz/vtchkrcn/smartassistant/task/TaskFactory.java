package cz.vtchkrcn.smartassistant.task;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import cz.vtchkrcn.smartassistant.task.model.Dashboard;
import cz.vtchkrcn.smartassistant.task.model.Event;
import cz.vtchkrcn.smartassistant.task.model.Note;
import cz.vtchkrcn.smartassistant.task.model.Task;

public class TaskFactory {

    private static final String TAG_DASHBOARD = "#DASHBOARD";
    private static final String TAG_EVENT = "#EVENT";

    public Task create(String data) throws ParseException {
        Task task = null;

        if (data.startsWith(TAG_DASHBOARD)) {
            String content = data.replace(TAG_DASHBOARD, "").trim();
            task = new Dashboard(content);
        } else if (data.startsWith(TAG_EVENT)) {
            data = data.replace(TAG_EVENT, "").trim();

            int dateIndex = data.indexOf(" ");
            String dateString = data.substring(0, dateIndex);

            data = data.substring(dateIndex).trim();
            int timeIndex = data.indexOf(" ");
            String time = data.substring(0, timeIndex);

            SimpleDateFormat parser = new SimpleDateFormat("dd.MM.yyyy HH:mm");
            Date date = parser.parse(dateString + " " + time);

            String content = data.substring(timeIndex).trim();
            task = new Event(content, date);
        } else {
            task = new Note(data.trim());
        }

        return task;
    }
}
