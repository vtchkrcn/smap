package cz.vtchkrcn.smartassistant;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.services.vision.v1.Vision;
import com.google.api.services.vision.v1.VisionRequestInitializer;
import com.google.api.services.vision.v1.model.AnnotateImageRequest;
import com.google.api.services.vision.v1.model.AnnotateImageResponse;
import com.google.api.services.vision.v1.model.BatchAnnotateImagesRequest;
import com.google.api.services.vision.v1.model.BatchAnnotateImagesResponse;
import com.google.api.services.vision.v1.model.Feature;
import com.google.api.services.vision.v1.model.Image;
import com.google.api.services.vision.v1.model.TextAnnotation;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

public class DetectionActivity extends AppCompatActivity {

    private static final int REQUEST_TASK = 1;

    private static final String GOOGLE_FEATURE_TYPE = "TEXT_DETECTION";

    private String GOOGLE_API_KEY;

    private File photo;

    private Button detectButton;
    private EditText dataText;
    private Button confirmButton;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detection);

        GOOGLE_API_KEY = getString(R.string.google_api_key);

        ImageView previewImage = findViewById(R.id.preview_image);
        detectButton = findViewById(R.id.detect_button);
        dataText = findViewById(R.id.data_text);
        confirmButton = findViewById(R.id.confirm_button);
        progressBar = findViewById(R.id.progress_bar);

        Intent intent = getIntent();
        String path = intent.getStringExtra("path");

        photo = new File(path);

        Bitmap temp = BitmapFactory.decodeFile(path);
        previewImage.setImageBitmap(temp);

        detectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                detectButton.setVisibility(View.INVISIBLE);
                progressBar.setVisibility(View.VISIBLE);
                new OCRTask().execute();
            }
        });

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!dataText.getText().toString().isEmpty()) {
                    Intent intent = new Intent(DetectionActivity.this, TaskActivity.class);
                    intent.putExtra("data", dataText.getText().toString());
                    startActivityForResult(intent, REQUEST_TASK);
                } else {
                    Toast.makeText(getApplicationContext(), "Cannot create empty task.", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void processDetection(String data) {
        data = data.replace("\n", "");
        progressBar.setVisibility(View.INVISIBLE);
        dataText.setText(data);
        dataText.setVisibility(View.VISIBLE);
        confirmButton.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_TASK) {
            String result = "";
            if (data != null) {
                result = data.getStringExtra("result");
            }

            if (resultCode == RESULT_OK) {
                Intent intent = new Intent();
                intent.putExtra("result", result);
                setResult(RESULT_OK, intent);
                finish();
            } else if (!result.isEmpty()) {
                Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
            }
        }
    }

    private class OCRTask extends AsyncTask<Void, Integer, String>
    {
        @Override
        protected String doInBackground(Void... values) {
            Log.d(OCRTask.class.toString(), "Running OCR task.");

//            return "#EVENT 13.1.2019 10:00 MEETING S KROCANEM";
            /**/
            String result = "";
            try {
                Log.i(OCRTask.class.toString(), "Sending image " + photo.getAbsolutePath() + " to remote api.");

                List<AnnotateImageResponse> responses = processRequest();

                for (AnnotateImageResponse res : responses) {
                    if (res.getError() != null) {
                        Log.e(OCRTask.class.toString(), res.getError().getMessage());
                    }

                    final TextAnnotation text = res.getFullTextAnnotation();

                    if (text != null) {
                        result = text.getText();
                        Log.d(OCRTask.class.toString(), "Detected text: " + result);
                    }
                }
            } catch(IOException e) {
                Log.e(OCRTask.class.toString(), e.getMessage());
                e.printStackTrace();
            }
            return result;
            /**/
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d(OCRTask.class.toString(), "OCR task done.");
            processDetection(result);
        }

        private List<AnnotateImageResponse> processRequest() throws IOException {
            Image inputImage = getImage();
            Feature feature = createFeature();

            AnnotateImageRequest request = new AnnotateImageRequest();
            request.setImage(inputImage);
            request.setFeatures(Arrays.asList(feature));

            BatchAnnotateImagesRequest batchRequest = new BatchAnnotateImagesRequest();
            batchRequest.setRequests(Arrays.asList(request));

            Vision vision = createVision();
            BatchAnnotateImagesResponse batchResponse = vision.images().annotate(batchRequest).execute();
            return batchResponse.getResponses();
        }

        private Vision createVision() {
            Vision.Builder visionBuilder = new Vision.Builder(
                    new NetHttpTransport(),
                    new AndroidJsonFactory(),
                    null);

            visionBuilder.setVisionRequestInitializer(new VisionRequestInitializer(GOOGLE_API_KEY));
            return visionBuilder.build();
        }

        private Image getImage() throws IOException {
            InputStream inputStream = new FileInputStream(photo);
            byte[] photoData = IOUtils.toByteArray(inputStream);
            inputStream.close();

            Image inputImage = new Image();
            inputImage.encodeContent(photoData);

            return inputImage;
        }

        private Feature createFeature() {
            Feature feature = new Feature();
            feature.setType(GOOGLE_FEATURE_TYPE);
            return feature;
        }
    }
}
